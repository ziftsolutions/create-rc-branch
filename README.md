Utility to create a release candidate branch
============================================

The Jenkins job does the following
- Clones the ziftsolutions.git repo's develop branch
- Get the latest RC branch name, e.g., RC-18.31
- Creates the next RC branch based on latest RC branch in previous step; in this case next branch is RC-18.32
- Pushes the newest RC branch to origin
