#!groovy

import groovy.json.*
import jenkins.*
import jenkins.model.*
import hudson.*
import hudson.model.*

gitRepo = env.GIT_REPO

// Add whichever params you think you'd most want to have
// replace the slackURL below with the hook url provided by
// slack when you configure the webhook
def notifySlack(text, channel) {
  def slackURL = 'https://hooks.slack.com/services/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'
  def payload = JsonOutput.toJson([text      : text,
                                   channel   : channel,
                                   username  : "jenkins",
                                   icon_emoji: ":jenkins:"])
  sh "curl -X POST --data-urlencode \'payload=${payload}\' ${slackURL}"
}
def cloneRepo()
{
  sh(returnStatus: true, script: """
       git config --global user.name "zift-ci"
       if [ -d ${env.WORKSPACE}/$gitRepo ]; then
         rm -rf ${env.WORKSPACE}/$gitRepo
       fi
     """)
  sshagent(['bitbucket-zift-ci']) {
    sh "git clone git@bitbucket.org:ziftsolutions/${gitRepo}.git"
  }
}

String getLatestRCBranchName()
{
/*
  // This command returns, for example
  // 92695ef7d82fe634389e7904d85de8be3dd7014f	refs/heads/release/RC-18.31
  sshagent(['bitbucket-zift-ci']) {
    def refs = sh(returnStdout: true, script: "cd $env.WORKSPACE/$gitRepo; git ls-remote -h -h | grep 'RC-'").split('/n')
  // If we have more than one RC-YY.nn branch, get the last one
    if (refs.size() > 1) {
      refs = refs[refs.size()-1]
    }
    refs = refs[0].split()
    def ref = refs[refs.size()-1].split('/')
    def branch = ref[ref.size()-1]
    return branch
  }
*/
  // Just read the Jenkins variables
  // staging_branch_local is of the form release/RC-YY.nn
  // Return the RC-YY.nn part
  String stBranch = env.staging_branch_local.tokenize('/')[1]
  return stBranch
}

// This function assumes that the RC branch name has the format
// RC-YY.nn, YY = year, nn = RC#, e.g., RC-18.31
//
// Note: nn=52 is the last week of the year. In this case,
// the function should return <next year>.01. For example,
// if latestRC = 19.52, then return 20.01.
String formulateNextRCBranchName(latestRC)
{
  // Get current year, e.g., 2018, year = ["2","0","1","8"]
  def year = Calendar.getInstance().get(Calendar.YEAR).toString().split("(?!^)")
  def yearYY = year[2]+year[3] // e.g., should be "18" in "2018"

  // Get the nn in RC-YY.nn and either set to 1 or add 1 to it
  def YYnn = latestRC.split('-')[1]
  def (YY, nn) = YYnn.tokenize('.')
  // If current year changed, or week# is 52, start the next year
  if (yearYY > YY || nn == '52') {
    YY = (YY.toInteger()+1).toString()
    nn = '01'
  }
  else {
    nn = (nn.toInteger()+1).toString()
    if (nn.toInteger() < 10) {
      // Prepend '0' to number if < 10 so branch name is
      // RC-19.02 instead of RC-19.2 for example
      nn = '0'+nn
    }
  }

  // Finally, form the new RC-YY.nn
  String newRC = "RC-"+yearYY+"."+nn
  return newRC
}

def createNextRCBranch(nextRC)
{
  def branch = sh(returnStatus: true, script: """
                    cd ${env.WORKSPACE}/$gitRepo
                    git checkout -b release/$nextRC
                  """)
  if (branch == 0) {
    sh(returnStatus: true, script: """
         cd $env.WORKSPACE/$gitRepo
         ls
         git status
       """)
    sshagent(['bitbucket-zift-ci']) {
      sh "cd ${env.WORKSPACE}/${gitRepo}; git push -u origin release/${nextRC}"
    }
  }
}

def storeVariable(String key, String value)
{
  Jenkins instance = Jenkins.getInstance()
  def globalNodeProperties = instance.getGlobalNodeProperties()
  def envVarsNodePropertyList = globalNodeProperties.getAll(hudson.slaves.EnvironmentVariablesNodeProperty.class)
  def newEnvVarsNodeProperty = null
  def envVars = null

  if ( envVarsNodePropertyList == null || envVarsNodePropertyList.size() == 0 ) {
    newEnvVarsNodeProperty = new hudson.slaves.EnvironmentVariablesNodeProperty();
    globalNodeProperties.add(newEnvVarsNodeProperty)
    envVars = newEnvVarsNodeProperty.getEnvVars()
} else {
    envVars = envVarsNodePropertyList.get(0).getEnvVars()
  }
  envVars.put(key, value)
  instance.save()
}

def storeNextRCGlobalVariable(nextRC)
{
  storeVariable("staging_branch", "origin/release/$nextRC")
  storeVariable("staging_branch_local", "release/$nextRC")
}

def storeProdRCGlobalVariable(prodRC)
{
  storeVariable("production_branch", "release/$prodRC")
}

def deletePrevRCBranch(latestRC)
{
  sh(returnStatus: true, script: """
       cd $env.WORKSPACE/$gitRepo
       ls
     """)
  sshagent(['bitbucket-zift-ci']) {
    sh "cd ${env.WORKSPACE}/$gitRepo; git push origin release/$latestRC --delete"
  }
}

String buildNode = 'linux-ubuntu'
if (env.BUILD_NODE) {
  buildNode = env.BUILD_NODE
}
node (buildNode) {
  timestamps {
    String latestRC = ''
    String nextRC = ''
    try {
      currentBuild.result = "SUCCESS"

      stage("Checkout") {
        checkout scm
      }

      stage("Clone ${gitRepo} repo") {
        cloneRepo()
      }

      stage("Get Latest RC branch name") {
        latestRC = getLatestRCBranchName()
        println "******************************************************"
        println "Latest RC branch: $latestRC"
        println "******************************************************"
      }

      stage("Formulate next RC branch name") {
        nextRC = formulateNextRCBranchName(latestRC)
        println "******************************************************"
        println "Next RC branch: $nextRC"
        println "******************************************************"
      }

      if (env.TEST_ONLY?.trim() != 'true') {
        stage("Create next RC branch") {
          createNextRCBranch(nextRC)
        }

        stage("Set global Jenkins staging branch variables") {
          storeNextRCGlobalVariable(nextRC)
        }

        stage("Set global Jenkins production variables") {
          storeProdRCGlobalVariable(latestRC)
        }

        stage("Trigger build and deploy $nextRC to staging") {
          build job: env.ST_JOB_TRIGGER, wait: false
        }

        stage("Trigger build $latestRC for production") {
          build job: env.PR_JOB_TRIGGER, wait: false
        }
      }
    }
    catch (org.jenkinsci.plugins.scriptsecurity.sandbox.RejectedAccessException e) {
      throw e
    }
    catch (err) {
      echo "ERROR: ${err}"
      currentBuild.result = 'FAILURE'
    }
    finally {
      echo "BUILD RESULT: ${currentBuild.result}"
      if (currentBuild.result == 'SUCCESS' && env.TEST_ONLY?.trim() != 'true') {
        String repo = "git@bitbucket.org:ziftsolutions/${gitRepo}.git"
        List<String> channels = ["#softwareengineer", "#team"]
        channels.each {
          notifySlack("Branches created for ${repo}.\nProduction branch: $latestRC\nStaging branch: $nextRC\n<${BUILD_URL}|Click here> for details!", it)
        }
      }
    }
  }
}
